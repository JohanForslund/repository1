// Johan Forslund
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

public class WebSaver {

	private String pageURL;
	private String fileName;
	private URL webSite;
	private File file;
	private ArrayList<String> text = new ArrayList<String>();
	private ArrayList<String> links = new ArrayList<String>();
	
	// Konstruktor som sparar URL och filnamnet.
	public WebSaver(String pageURL, String fileName){
		this.pageURL = pageURL;
		this.fileName = fileName;
	}
	
	public void fetchAndSave() throws IOException{
		webSite = new URL(pageURL); // URL:en fr�n "String pageURL" sparas i variablens webSite.
		file = new File(fileName); // Ny fil skapas med namnet fr�n variabeln "fileName".
		FileUtils.copyURLToFile(webSite, file); // H�r h�mtas sidan och kopieras till "file".
	}
	
	public void printLinks() throws IOException, UnknownHostException{
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));	// BufferedReader och FileReader skapas f�r att kunna l�sa fr�n filen.
		
		String line;
		while ((line = bufferedReader.readLine()) != null){		// BufferedReader l�ser varje rad, och om raden i filen != null, lagras den i variabeln "line".
			text.add(line);		// H�r l�ggs raden till i en ArrayList.
		}
		bufferedReader.close(); // N�r den l�st in allt till ArrayListen "text" st�ngs bufferedreader.
		
		for(int c = 0; c < text.size(); c++){
			if (text.get(c).contains("a href")){ 	// H�r loopas varje rad i ArrayListen igenom och kollar om den inneh�ller "a href".
				String[] parts = text.get(c).split("\"");	// Om s� �r fallet delas texten d�r det finns ett citattecken och lagras i arrayen "parts". Eftersom texten som skall skrivas ut st�r inom citattecken.
				for (int i = 0; i < parts.length; i++){		// Eftersom vi inte kan veta hur m�nga delar den har delats upp i loopar vi igenom arrayen "parts".
					if (parts[i].contains(".html")){		// Om parts[i] skulle inneh�lla ".html" l�ggs den till i ArrayListen links.
						links.add(parts[i]);		// Parts[i] l�ggs till.
					}
				}
			}
		}
		
		System.out.println(webSite);
		
		for (String s : links){		// H�r skrivs den ut!
			System.out.println(s);
		}
	}
}
