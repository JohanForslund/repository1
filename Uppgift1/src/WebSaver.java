// Johan Forslund
import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;

public class WebSaver {

	private String pageURL;
	private String fileName;
	
	// Konstruktor som sparar URL och filnamnet.
	public WebSaver(String pageURL, String fileName){
		this.pageURL = pageURL;
		this.fileName = fileName;
	}
	
	public void fetchAndSave() throws IOException{
		URL webSite = new URL(pageURL); // URL:en fr�n "String pageURL" sparas i variablens webSite.
		File file = new File(fileName); // Ny fil skapas med namnet "String fileName".
		FileUtils.copyURLToFile(webSite, file); // H�r kopieras "webSite" till filen "file".
	}
}
