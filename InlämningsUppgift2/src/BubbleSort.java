// Johan Forslund
public class BubbleSort {
	
	private String[] myList;
	private int len;
	
	// H�r sparas arrayen n�r ett objekt skapas. Programmet h�mtar �ven l�ngden p� arrayen som kommer anv�ndas senare.
	public BubbleSort(String[] array){
		myList = array;
		len = myList.length;
	}

	// N�r metoden sortNumber v�l anropas finns redan myList och l�ngden p� den.
	public String[] sortWords(){
		for(int i = len-1; i >= 0; i--) {						// H�r skapas en for-loop som pekar p� sista ordet i arrayen och som minskar med ett tills arrayen �r slut.
		    for(int j = 0; j < i; j++) {						// H�r skapas en till loop som pekar p� det f�rsta ordet i arrayen. Den kommer �ka tills den kommer dit d�r den f�rsta pekaren, (i), befinner sig.
		        if(myList[j].compareToIgnoreCase(myList[j + 1]) > -1) {		// H�r j�mf�r den f�rsta bokstaven i orden i decimal enligt ASCII-tabellen. Om t.ex. (j) pekar p� "a" (97 i decimal), och (j+1) pekar p� "c" (99 i decimal), kollar den 97-99 vilket blir -2. -2 > -1 och d�rf�r utf�rs inneh�llet i if-satsen.
		            String tmp = myList[j];						// Om villkoret i if-satsen == true, sparas ordet som (j) pekar p� i variabeln (tmp).
		            myList[j] = myList[j + 1];					// Ordet som stod efter (j) sparas ist�llet d�r (j) pekar.
		            myList[j + 1] = tmp;						// Sedan sparas v�rdet i (tmp) i platsen efter (j).
		        }												// Det som har h�nt �r att de b�da orden har bytt plats.
		    }													// N�r (j) slutligen kommer fram till i, minskar v�rdet i med 1. (j)- pekaren b�rjar ocks� om och forts�tter �terigen tills den kommer till (i).
		}
		// N�r (i) slutligen g�tt igenom hela arrayen ska den returneras.
		return myList;
	}
	
	
}
