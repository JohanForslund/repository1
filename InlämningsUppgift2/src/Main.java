// Johan Forslund

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		String[] myList = new String[10000];
		BufferedReader br = new BufferedReader(new FileReader("src/words.txt"));
		
		String line;
		for (int counter = 0; counter < 10000; counter++){ // I denna for-loop l�ser den in samtliga ord fr�n words.txt in i mylist-arrayen.
			myList[counter] = (line = br.readLine());
		}
		br.close();
		
		BubbleSort bubbleSort = new BubbleSort(myList);		// H�r skapas ett objekt av bubblesort och myList skickas till konstruktorn.
		myList = bubbleSort.sortWords();	// H�r anropas metoden sortWords. Metoden kommer ocks� returnera den sorterade arrayen till myList som vi skapade innan.
		
		
		BufferedWriter out = new BufferedWriter(new FileWriter("src/words_sorted.txt"));	// H�r skapas ett .txt dokument d�r de sorterade orden kommer skrivas ut.
		for (int counter = 0; counter < 10000; counter++){
			out.write(myList[counter] + "\n");					//H�r skrivs varje ord i arrayen som har plats(counter) ut till words_sorted.txt.
		}
		out.close();
		
	}

}
