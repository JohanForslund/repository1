// Johan Forslund

/* 
 * Jag tog bort en stor del av bubblesort klassen och l�t ist�llet datorn slumpa fram talen sj�lv.
 * Sedan skapade jag klassen bubblesort som sorterar och returnerar talen.
*/

import java.util.Arrays;


public class Main {
	
	public static void main(String[] args) {
		int[] myList = new int[10000];
		
		// H�r slumpas 10 000 tal fram med v�rden mellan 1 till och med 1000.
		for (int c = 0; c < myList.length; c++){
			myList[c] = (int)(1+ Math.random() * 1000);
		}
		
		// H�r skapas ett objekt av BubbleSort och skickar in myList-arrayen till konstruktorn.
		BubbleSort bubbleSort = new BubbleSort(myList);
		// H�r anropas metoden sortNumbers i bubbleSort, som kommer returnera arrayen med siffrorna i ording, vilket sparas i myList-arrayen.
		myList = bubbleSort.sortNumbers();
		System.out.println(Arrays.toString(myList));
	}

}
 