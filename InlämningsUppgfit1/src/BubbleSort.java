// Johan Forslund
public class BubbleSort {
	
	private int[] myList;
	private int len;
	
	// H�r sparas arrayen n�r ett objekt skapas. Programmet h�mtar �ven l�ngden p� arrayen som kommer anv�ndas senare.
	public BubbleSort(int[] array){
		myList = array;
		len = myList.length;
	}

	// N�r metoden sortNumber v�l anropas finns redan myList och l�ngden p� den.
	public int[] sortNumbers(){
		
		for (int i = 0; i < len; i++){			// H�r skapas en for-loop som pekar p� f�rsta talet i arrayen (myList[0]), som �kar med ett tills arrayen �r slut.
			for (int j = len-1; j > i; j--){	// H�r skapas en till loop som pekar p� det sista talet i arrayen. Den kommer minska tills den kommer dit d�r den f�rsta pekaren, (i), befinner sig.
				if (myList[j] < myList[j-1]){	// H�r i if-satsen kollar den f�rst om talet som (j) pekar p� �r mindre �n talet som st�r f�re.
					int tmp = myList[j];		// Om villkoret i if-satsen == true, sparas talet som (j) pekar p� i variabeln (tmp).
					myList[j] = myList[j-1];	// Talet som stod f�re (j) sparas ist�llet d�r (j) pekar.
					myList[j-1] = tmp; 			// Sedan sparas v�rdet i (tmp) i platsen f�re (j).
				}								// Det som har h�nt �r att de b�da talen har bytt plats.
			}	// N�r (j) slutligen kommer fram till i, �kar v�rdet i med 1. (j)- pekaren b�rjar ocks� om och forts�tter �terigen tills den kommer till (i).
		}
		// N�r i slutligen g�tt igenom hela arrayen ska den returneras.
		return myList;
	}
}
